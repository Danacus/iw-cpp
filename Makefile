cpp-iw: src/cpp.c src/util.c src/util.h src/list.c src/list.h src/comments.c src/comments.h src/commands.c src/commands.h src/shared.h
	gcc -g -std=c99 -Wall src/cpp.c src/util.c src/list.c src/comments.c src/commands.c -o cpp-iw

clean:
	rm -f cpp-iw
