#ifndef CPP_H_INC
#define CPP_H_INC

char *str_to_heap(char *str);
void process_line(char *line);
void process_command(char *command);
void show_warning_error(char *warn_err);
void define_const();
void undef_const();
void option(char c);

#endif