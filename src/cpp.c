#include "cpp.h"
#include "list.h"
#include "util.h"
#include "comments.h"
#include "commands.h"
#include "shared.h"

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Program information */
const char *VERSION = "1.0.1";
const char *AUTHOR = "Daan Vanoverloop";

int experimental_mode = 0;

/*
 * Pre-process one given line of the input file by writing the desired output
 * to stdout (use printf), and any errors to stderr (use pr_error).
 *
 * Removes:
 * - Whitespace
 * - Tabs
 * - Single-line comments
 * - Multi-line comments
 *
 * Commands:
 * - #warning <message>
 * - #error <message>
 * - #define <key> <value>
 * - #undef <key>
 */
void process_line(char *line) {
  // Tokenize the current line with tab or space as delimiter
  // It basically means all tabs and spaces will be ignored,
  // and the tokens will be returned one by one,
  // so we can reconstruct the line.

  // We need to use a copy of the line, 
  // because some functions need the original line 
  // Edit: I lied, we don't char *linecpy = str_to_heap(line);

  for (char *token = strtok(line, " \t"); token; token = strtok(NULL, " \t")) {
    // First we will see if the current token is an alias
    char *alias_value = alias_list_lookup(aliases, token);

    if (alias_value != NULL) {
      // If so, we can replace the current token by that value
      token = alias_value;
    }

    if (check_multi_comment(&token))
      continue;

    // If the token is an empty string, make sure to move to the next token,
    // to prevent useless spaces
    if (strlen(token) == 0)
      continue;

    if (check_single_comment(token))
      break;

    if (token[0] == '#') {
      // It's a command! Let's call process_command, he likes commands!
      process_command(token);

      // After we've processed it, we can move on to the next line
      break;
    }

    if (token[strlen(token) - 1] == '\n') {
      // If there is a newline character at the end of the token,
      // we don't have to add another space
      printf("%s", token);
    } else {
      // Otherwise we can add a space to make sure the tokens stay 'seperated'
      printf("%s ", token);
    }

    // At the end, get the next token
    // When we use the NULL pointer as first argument,
    // it will give us the next token in the last string we gave him.
  }
}


/*
 * Process a single-char "-C" command line option.
 *
 * TODO add your own single-char options here
 *  (e.g., "-v" for printing author and version information)
 */
void option(char c) {
  switch (c) {
  case 'h':
    printf("cpp-iw: The awesome IW C preprocessor\n");
    printf("\nUsage: cpp-iw [-h -e] file.c\n");
    printf("\nRecognized options:\n");
    printf("-h\tdisplay this help message\n");
    printf("-e\tterminate preprocessing on #error\n");
    printf("-v\tshow author and version information\n");
    exit(0);
    break;
  case 'e':
    exit_on_error = 1;
    break;
  case 'v':
    printf("C Preprocessor by %s\n", AUTHOR);
    printf("\nVersion: %s\n", VERSION);
    exit(0);
    break;
  case 'u':
    experimental_mode = 1;
    break;
  default:
    pr_error("Unrecognized option '%c'\n", c);
    exit(1);
  }
}

int main(int argc, char *argv[]) {
  int i;
  char *s;

  /* Process command line options */
  for (i = 1; i < argc && *argv[i] == '-'; i++)
    option(*(argv[i] + 1));

  if (argv[i] == NULL) {
    pr_error("No input C file provided\n");
    return 1;
  }

  /* Open the input file */
  if (infile_open(argv[i])) {
    pr_error("Could not open input file '%s'\n", argv[i]);
    return 1;
  }

  /* First pass the list test suite (make sure to add your own tests!) */
  if (alias_list_test() == 0)
    return 1;

  /* Do the actual preprocessing work line by line */
  aliases = alias_list_create();

  while ((s = infile_getline())) {
    process_line(s);
  }

  infile_close();
  alias_list_delete(aliases);
  return 0;
}
