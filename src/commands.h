#ifndef COMMANDS_H_INC
#define COMMANDS_H_INC

char *readKey();
char *readValue();
void process_command(char *command);
void show_warning_error(char *warn_err);
void define_const();
void undef_const();

#endif