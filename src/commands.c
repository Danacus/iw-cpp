#include "commands.h"
#include "list.h"
#include "shared.h"
#include "util.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct alias_list *aliases = NULL;
int exit_on_error = 0;

void process_command(char *command) {
  // Remove the newline character if necessary by replacing it with a null
  // terminator
  if (command[strlen(command) - 1] == '\n')
    command[strlen(command) - 1] = '\0';

  // If only switches would work on strings, it would look so much cleaner
  if (strcmp(command, "#warning") == 0) {

    show_warning_error("warning: ");

  } else if (strcmp(command, "#error") == 0) {

    show_warning_error("error: ");

    if (exit_on_error)
      exit(1);

  } else if (strcmp(command, "#define") == 0) {

    define_const();

  } else if (strcmp(command, "#undef") == 0) {

    undef_const();

  } else {

    pr_error("error: invalid preprocessing directive: %s\n", command);

    if (exit_on_error)
      exit(1);
  }
}

/*
 * Prints the rest of the line to stderr, after printing its first argument
 */
void show_warning_error(char *warn_err) {
  pr_error(warn_err);

  // Remember that strtok "maintains internal state"!
  // That's why we don't need to pass the line to this function

  for (char *token = strtok(NULL, " \t"); token; token = strtok(NULL, " \t")) {
    if (token[strlen(token) - 1] == '\n') 
      pr_error("%s", token);
    else 
      pr_error("%s ", token);
  }
}

char *readKey() {
  char *key = strtok(NULL, " \t\n");

  if (key != NULL) {
    // Only copy the key to the heap when it exists
    // I'm doing this because of how strtok works sometimes
    // It's just safer I guess
    key = str_to_heap(key);
  } else {
    pr_error("%s\n", "warning: missing key");
  }

  return key;
}

char *readValue() {
  char *value = strtok(NULL, "\n");

  if (value != NULL) {
    // We should remove any spaces or tabs
    // at the start of the string
    while (*value == ' ' || *value == '\t') 
      value++;
    
    value = str_to_heap(value);
  } else {
    // When there is no key, we can throw
    // an empty string on the heap
    value = str_to_heap("");
  }

  return value;
}

/*
 * Adds a key/value pair (provided by strtok) to the alias list
 */
void define_const() {
  char *key = readKey();
  char *value = readValue();

  if (key != NULL && value != NULL) {
    char *old_value = alias_list_lookup(aliases, key);

    if (old_value != NULL) {
      if (strcmp(old_value, value) != 0) {
        pr_error("warning: '%s' redefined\n", key);

        // We don't need to copy the value to a safe new place on the heap,
        // because alias_list_set will take care of that and fix
        // any potential memory leaks
        // This is important, because value will be freed at the end of this
        // function, so it must be copied somehow
        alias_list_set(aliases, key, value);
      }
    } else {
      alias_list_append(aliases, key, value);
    }

    free(key);
    free(value);
  }
}

/*
 * Removes the current key (provided by strtok) from the list of aliases
 */
void undef_const() {
  char *key = readKey();

  if (key != NULL) {
    alias_list_remove(aliases, key);

    free(key);
  }
}