#include "comments.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int removing_comment = 0;

/*
 * Removes single-line comments
 * When 1 is returned, we should break to the next line
 */
int check_single_comment(char *token) {
  if (strlen(token) > 1 && token[0] == '/' && token[1] == '/') {
    // Whenever we find the start of a comment, stop printing tokens in this
    // line
    printf("%s", "\n");
    return 1;
  }

  return 0;
}

/*
 * Removes multi-line comments
 * When 1 is returned, we should continue to the next token
 */
int check_multi_comment(char **token) {
  // We need a pointer to a pointer if we want to edit the token
  // If it's a multi-line comment, we need to set a global variable
  // I'm also checking the length first to prevent segfaults (references to invalid addresses)
  if (strlen(*token) > 1 && (*token)[0] == '/' && (*token)[1] == '*') {
    removing_comment = 1;
  } else if (strlen(*token) > 1 && (*token)[0] == '*' && (*token)[1] == '/') {
    // We are at the end of the comment
    removing_comment = 0;

    // We can simply add 2 to the pointer to skip the first two characters
    *token += 2;
  }
  
  // Skip the tokens for as long as we are in a comment block
  return removing_comment;
}