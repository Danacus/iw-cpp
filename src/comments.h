#ifndef COMMENTS_H_INC
#define COMMENTS_H_INC

int check_single_comment(char *token);
int check_multi_comment(char **token);

#endif