#ifndef SHARED_H_INC
#define SHARED_H_INC

// The preprocessor will not include this header twice,
// because there is an if statement in list.h to prevent this
#include "list.h"

extern struct alias_list *aliases;
extern int exit_on_error;
extern int experimental_mode;

#endif